'''shove-sqlalchemy fabfile'''

from fabric.api import local


def release():
    '''release shove-sqlalchemy'''
    local('jython setup.py bdist_wheel sdist --format=gztar,bztar,zip upload')

